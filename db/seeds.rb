# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

Employee.delete_all

1000.times.each do |n|
  name_array = %W[Trish Bob Hope Lisa Clay Ronald Mike Ben Jon Linda Lindsay Banks Brad Amy Joe Bill Taylor Dave]
  employees = Employee.create ([{rate: rand(10..100), name: name_array.shuffle[0]}])


end


